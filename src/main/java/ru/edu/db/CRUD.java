
package ru.edu.db;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CRUD {

    /**
     * Выражение SQL.
     * Выбор всех записей.
     */
    public static final String SELECT_ALL_SQL =
            "SELECT * FROM records";

    /**
     * Выражение SQL.
     * Выбор записи по id.
     */
    public static final String SELECT_BY_ID =
            "SELECT * FROM records WHERE id=?";

    /**
     * Выражение SQL.
     * Вставка записи.
     */
    public static final String INSERT_SQL =
            "INSERT INTO records"
                    + "(title, type, text, price,"
                    + " publisher, email, phone, picture_url)"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Выражение SQL.
     * Обновление требуемых записей.
     */
    public static final String UPDATE_SQL =
            "UPDATE records SET"
                    + "title=?,"
                    + "type=?, text=?, "
                    + "price=?, "
                    + "publisher=?,"
                    + "email=?, "
                    + "phone=?,"
                    + "picture_url=?"
                    + "WHERE id=?";

    /**
     * Экземпляр CRUD.
     */
    private static CRUD instance;

    /**
     * dataSource.
     */
    private DataSource dataSource;

    /**
     * Счетчик записей.
     */
    private static Long idCount = 0L;

    /**
     * Конструктор для dataSource.
     * @param dataSourceInc - dataSource
     */
    public CRUD(final DataSource dataSourceInc) {
        this.dataSource = dataSourceInc;
    }

    /**
     * Setter для проведения тестирования.
     *
     * @param crud - Экземпляр CRUD.
     */
    public static void setInstance(final CRUD crud) {
        instance = crud;
    }

    /**
     * Синглтон.
     *
     * Т.к. у нас есть критическая секция,
     * в неё будет попадать строго один поток.
     *
     * @return instance - экземпляр CRUD.
     */
    public static CRUD getInstance() {

        synchronized (CRUD.class) {
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env
                            = (Context) ctx.lookup("java:/comp/env");
                    DataSource dataSource
                            = (DataSource) env.lookup("jdbc/dbLink");
                    instance = new CRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }


    /**
     * Метод по получению всех записей из БД.
     * @return - все записи.
     */
    public List<Record> getIndex() {
        return query(SELECT_ALL_SQL);
    }


    private List<Record> query(final String sql, final Object... values) {

        try (PreparedStatement statement
                     = getConnection().prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();

            List<Record> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Record map(final ResultSet resultSet) throws SQLException {

        Record record = new Record();
        record.setId(resultSet.getLong("id"));
        record.setTitle(resultSet.getString("title"));
        record.setType(resultSet.getString("type"));
        record.setPrice(new BigDecimal(resultSet.getString("price")));
        record.setText(resultSet.getString("text"));
        record.setPublisher(resultSet.getString("publisher"));
        record.setEmail(resultSet.getString("email"));
        record.setPhone(resultSet.getString("phone"));
        record.setPictureUrl(resultSet.getString("picture_url"));
        return record;
    }

    private Connection getConnection() {

        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }


    /**
     * Получение записи из БД по ID.
     *
     * @param id - ID.
     * @return - запись БД.
     */
    public Record getById(final String id) {
        List<Record> records = query(SELECT_BY_ID, id);
        if (records.isEmpty()) {
            return null;
        }
        return records.get(0);
    }


    private int execute(final String sql, final Object... values) {

        try (PreparedStatement statement
                     = getConnection().prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    /**
     * Сохранение записи в БД.
     * @param record - объект-запись в БД.
     * @return id - ID.
     */
    public Long save(final Record record) {

        if (record.getId() == null) {
            Long id = idCount;
            idCount++;
            insert(record);
            return id;
        } else {
            update(record);
            return record.getId();
        }
    }


    /**
     * Обновление записи в БД.
     *
     * @param record - объект-запись в БД.
     */
    private void update(final Record record) {
        execute(UPDATE_SQL,
                record.getTitle(),
                record.getType(),
                record.getText(),
                record.getPrice(),
                record.getPublisher(),
                record.getEmail(),
                record.getPhone(),
                record.getPictureUrl(),
                record.getId());
    }


    /**
     * Вставка записи в БД.
     *
     * @param record - объект-запись в БД.
     */
    private void insert(final Record record) {
        execute(INSERT_SQL,
                record.getTitle(),
                record.getType(),
                record.getText(),
                record.getPrice(),
                record.getPublisher(),
                record.getEmail(),
                record.getPhone(),
                record.getPictureUrl());
    }
}
