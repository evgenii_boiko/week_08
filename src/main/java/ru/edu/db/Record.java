package ru.edu.db;

import java.math.BigDecimal;

public class Record {

    /**
     * ID объявления в БД.
     */
    private Long id;

    /**
     * Название объявления.
     */
    private String title;

    /**
     * Тип объявления.
     */
    private String type;

    /**
     * Цена.
     */
    private BigDecimal price;

    /**
     * Текст объявления.
     */
    private String text;

    /**
     * Имя продавца.
     */
    private String publisher;

    /**
     * E-mail продавца.
     */
    private String email;

    /**
     * Телефон продавца.
     */
    private String phone;

    /**
     * URL картинки объявления.
     */
    private String pictureUrl;


    /**
     * Получение ID объявления в БД.
     *
     * @return id - ID объявления в БД.
     */
    public Long getId() {
        return id;
    }

    /**
     * Задание ID объявления в БД.
     *
     * @param idInc - ID объявления в БД.
     */
    public void setId(final Long idInc) {
        this.id = idInc;
    }

    /**
     * Получение названия объявления.
     *
     * @return - Название объявления.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Задание названия объявления.
     *
     * @param titleInc - Название объявления.
     */
    public void setTitle(final String titleInc) {
        this.title = titleInc;
    }

    /**
     * Получение типа объявления.
     *
     * @return - Тип объявления.
     */
    public String getType() {
        return type;
    }

    /**
     * Задание типа объявления.
     *
     * @param typeInc - Тип объявления.
     */
    public void setType(final String typeInc) {
        this.type = typeInc;
    }

    /**
     * Получение цены.
     *
     * @return - цена.
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Задание цены.
     *
     * @param priceInc - цена.
     */
    public void setPrice(final BigDecimal priceInc) {
        this.price = priceInc;
    }

    /**
     * Получение текста объявления.
     *
     * @return - текст объявления.
     */
    public String getText() {
        return text;
    }

    /**
     * Задание текста объявления.
     *
     * @param textInc - текст объявления.
     */
    public void setText(final String textInc) {
        this.text = textInc;
    }

    /**
     * Получение имени продавца.
     *
     * @return - Имя продавца.
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Задание имени продавца.
     *
     * @param publisherInc - Имя продавца.
     */
    public void setPublisher(final String publisherInc) {
        this.publisher = publisherInc;
    }

    /**
     * Получение E-mail'а продавца.
     *
     * @return - E-mail продавца.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Задание E-mail'а продавца.
     *
     * @param emailInc - E-mail продавца.
     */
    public void setEmail(final String emailInc) {
        this.email = emailInc;
    }

    /**
     * Получение телефона продавца.
     *
     * @return - Телефон продавца.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Задание телефона продавца.
     *
     * @param phoneInc - Телефон продавца.
     */
    public void setPhone(final String phoneInc) {
        this.phone = phoneInc;
    }

    /**
     * Получение URL картинки объявления.
     *
     * @return - URL картинки объявления.
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * Задание URL картинки объявления.
     *
     * @param pictureUrlInc - URL картинки объявления.
     */
    public void setPictureUrl(final String pictureUrlInc) {
        this.pictureUrl = pictureUrlInc;
    }

    /**
     * Получение краткого описания.
     *
     * @param length - длина описания
     * @return - подстрока описания.
     */
    public String getShort(final int length) {
        if (text.length() <= length) {
            return text;
        }
        return text.substring(0, length) + "...";
    }
}
