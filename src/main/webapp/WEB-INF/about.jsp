<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<html>
<head>
    <jsp:include page="/WEB-INF/templates/header.jsp">
        <jsp:param name="title" value="About"/>
    </jsp:include>
</head>
<body>
<div class="page">
    <div class="header">
        <h1>Об авторе</h1>
    </div>
    <div class="content">
        <p>Евгений Бойко</p>
        <p><a href="https://bitbucket.org/evgenii_boiko/">Ссылка на проект битбакет</a>
    </div>
    <%@ include file="/WEB-INF/templates/footer.jsp" %>
</div>
</body>
</html>