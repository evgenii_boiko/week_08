package ru.edu.db;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CRUDTest {

    private static DataSource dataSourceMock = mock(DataSource.class);

    private Connection connectionMock = mock(Connection.class);

    private static ThreadLocal<Connection> threadLocalConnection = new InheritableThreadLocal<>();

    private CRUD instance;

    /**
     * Выражение SQL.
     * Выбор всех записей.
     */
    public static final String SELECT_ALL_SQL = "SELECT * FROM records";

    /**
     * Выражение SQL.
     * Выбор записи по id.
     */
    public static final String SELECT_BY_ID = "SELECT * FROM records WHERE id=?";

    /**
     * Выражение SQL.
     * Вставка записи.
     */
    public static final String INSERT_SQL =
            "INSERT INTO records"
                    + "(title, type, text, price, publisher, email, phone, picture_url)"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Выражение SQL.
     * Обновление требуемых записей.
     */
    public static final String UPDATE_SQL =
            "UPDATE records SET"
                    + "title=?,"
                    + "type=?, text=?, "
                    + "price=?, "
                    + "publisher=?,"
                    + "email=?, "
                    + "phone=?,"
                    + "picture_url=?"
                    + "WHERE id=?";

    private PreparedStatement selectByIdStatement = mock(PreparedStatement.class);
    private PreparedStatement insertStatement = mock(PreparedStatement.class);
    private PreparedStatement selectAllStatement = mock(PreparedStatement.class);
    private PreparedStatement updateStatement = mock(PreparedStatement.class);

    private ResultSet selectIdResultset = mock(ResultSet.class);
    private ResultSet insertResultset = mock(ResultSet.class);
    private ResultSet selectAllResultset = mock(ResultSet.class);
    private ResultSet updateResultset = mock(ResultSet.class);

    private Record recordMock1 = mock(Record.class);
    private Record recordMock2 = mock(Record.class);
    private Record create = mock(Record.class);
    private Record update = mock(Record.class);

/**
     * Подключаем зависимость:
     *         <dependency>
     *             <groupId>com.github.h-thurow</groupId>
     *             <artifactId>simple-jndi</artifactId>
     *             <version>0.23.0</version>
     *             <scope>test</scope>
     *         </dependency>
     * @throws NamingException
     */

    @BeforeClass
    public static void setupJndiContext() throws NamingException, SQLException {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");
        System.setProperty("org.osjava.sj.jndi.shared", "true");

        Context ic = new InitialContext();
        ic.rebind("java:/comp/env", ic);
        ic.rebind("java:/comp/env/jdbc/dbLink", dataSourceMock);
        ic.rebind("jdbc/dbLink", dataSourceMock);

        Context ctx = new InitialContext();
        Context env  = (Context)ctx.lookup("java:/comp/env");
        DataSource dataSource = (DataSource) env.lookup("jdbc/dbLink");
        assertEquals(dataSourceMock, dataSource);

        when(dataSource.getConnection()).thenAnswer((invocation) -> threadLocalConnection.get());
    }

    @Before
    public void setupJndi() throws NamingException, SQLException {
        threadLocalConnection.set(connectionMock);
    }

    @Before
    public void setup() throws NamingException, SQLException {

        when(connectionMock.prepareStatement(SELECT_ALL_SQL)).thenReturn(selectAllStatement);
        when(connectionMock.prepareStatement(SELECT_BY_ID)).thenReturn(selectByIdStatement);
        when(connectionMock.prepareStatement(INSERT_SQL)).thenReturn(insertStatement);
        when(connectionMock.prepareStatement(UPDATE_SQL)).thenReturn(updateStatement);


        when(selectAllStatement.executeQuery()).thenReturn(selectAllResultset);
        when(selectAllResultset.next()).thenReturn(true).thenReturn(true).thenReturn(false);


        when(selectAllResultset.getString("id")).thenReturn("10").thenReturn("11");
        when(selectAllResultset.getString("title")).thenReturn("titleTest1").thenReturn("titleTest2");
        when(selectAllResultset.getString("type")).thenReturn("typeTest1").thenReturn("typeTest2");
        when(selectAllResultset.getDouble("price")).thenReturn(0.01).thenReturn(0.02);
        when(selectAllResultset.getString("text")).thenReturn("textTest1").thenReturn("textTest2");
        when(selectAllResultset.getString("publisher")).thenReturn("publisherTest1").thenReturn("publisherTest2");
        when(selectAllResultset.getString("email")).thenReturn("emailTest1").thenReturn("emailTest2");
        when(selectAllResultset.getString("phone")).thenReturn("phoneTest1").thenReturn("phoneTest2");
        when(selectAllResultset.getString("picture_url")).thenReturn("pictureTest1").thenReturn("pictureTest2");


        when(selectByIdStatement.executeQuery()).thenReturn(selectIdResultset);
        when(selectIdResultset.next()).thenReturn(true).thenReturn(false);
        when(selectIdResultset.getLong("id")).thenReturn(13L);
        when(selectIdResultset.getString("title")).thenReturn("titleTest3");
        when(selectIdResultset.getString("type")).thenReturn("typeTest3");
        when(selectIdResultset.getString("price")).thenReturn("3");
        when(selectIdResultset.getString("text")).thenReturn("textTest3");
        when(selectIdResultset.getString("publisher")).thenReturn("publisherTest3");
        when(selectIdResultset.getString("email")).thenReturn("emailTest3");
        when(selectIdResultset.getString("phone")).thenReturn("phoneTest3");
        when(selectIdResultset.getString("picture_url")).thenReturn("pictureTest3");


        when(insertStatement.executeQuery()).thenReturn(insertResultset);
        when(insertResultset.next()).thenReturn(true).thenReturn(false);


        when(updateStatement.executeQuery()).thenReturn(updateResultset);
        when(updateResultset.next()).thenReturn(true).thenReturn(false);
        when(updateResultset.getLong("id")).thenReturn(1L);

        instance = CRUD.getInstance();
    }

    @Test
    public void getInstance() throws SQLException {

        PreparedStatement preparedStatementMock = mock(PreparedStatement.class);
        when(connectionMock.prepareStatement(any(String.class))).thenReturn(preparedStatementMock);

        preparedStatementMock.executeQuery();


        Record recordForUpdate = new Record();

        recordForUpdate.setTitle("title");
        recordForUpdate.setType("type");
        recordForUpdate.setId(0L);
        Long idOfUpdatedRecord = instance.save(recordForUpdate);
        System.out.println("idOfUpdatedRecord: " + idOfUpdatedRecord);

        verify(preparedStatementMock, times(1)).setObject(1, "title");
        verify(preparedStatementMock, times(1)).setObject(2, "type");

        assertNotNull(instance);


        Record recordForInsert = new Record();

        recordForInsert.setTitle("title");
        recordForInsert.setType("type");
        recordForInsert.setText("text");
        recordForInsert.setPrice(BigDecimal.TEN);
        recordForInsert.setPublisher("publisher");
        recordForInsert.setEmail("email");
        recordForInsert.setPhone("phone");
        recordForInsert.setPictureUrl("picture_url");


        Long idOfInsertedRecord = instance.save(recordForInsert);
        System.out.println("idOfInsertedRecord: " + idOfInsertedRecord);
        System.out.println(recordForInsert.getId());

        verify(preparedStatementMock, times(2)).setObject(1, "title");
        verify(preparedStatementMock, times(2)).setObject(2, "type");
    }

    @Test
    public void getById() throws SQLException {

        Record recordForInsert = new Record();

        recordForInsert.setId(null);

        recordForInsert.setTitle("title");
        recordForInsert.setType("type");
        recordForInsert.setText("text");
        recordForInsert.setPrice(BigDecimal.TEN);
        recordForInsert.setPublisher("publisher");
        recordForInsert.setEmail("email");
        recordForInsert.setPhone("phone");
        recordForInsert.setPictureUrl("picture_url");


        instance.save(recordForInsert);

        ResultSet resultSetMock = mock(ResultSet.class);

        instance.getById("0");
    }

    @Test
    public void save() throws SQLException {

        when(create.getId()).thenReturn(null);
        when(create.getTitle()).thenReturn("Title1");
        when(update.getId()).thenReturn(null);
        when(update.getType()).thenReturn("Type");

        when(update.getType()).thenReturn("Type");


        Long id = instance.save(create);
        Long id2 = instance.save(update);

        assertEquals(Long.valueOf(2), id);
        assertEquals(Long.valueOf(3), id2);

    }

    @Test
    public void setSelectById() {
        recordMock2 = instance.getById("1L");
        assertEquals("titleTest3", recordMock2.getTitle());
    }
}
